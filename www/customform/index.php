<html>
	<head>
		<title>awe.sm Custom Link Creator</title>
		<meta name="description" content="An interface for existing awe.sm users to create custom tracking links.">
	</head>
	<body>
		<div id="container">
			
						
			<!-- Bookmarklet code to be completed
			<div id="bookmarklet">
				<p class="label">Grab a bookmarklet:</p>
				<p class="label"><a href="javascript:(function()%20{window.open('http://developers.awe.sm/tools/customlinkbuilder/?target='+encodeURIComponent(location.href));})();">awe.sm Custom</a></p>
				<p class="description">Drag the above link into your bookmarks bar.</p>
			</div> 
			-->


			<div id="content">
<h1>awe.sm Custom Link Creator</h1>

				<form method="get">
<h3>Required Fields:</h3>
					<p class="label">Original URL*:
					<input id="target" name="target" type="text" /></p>
					<p class="label">Custom Link Stub*: 		
					<input id="awesm_id" name="awesm_id" type="text"/>
					<span class="description"><em>An error will be returned if the stub is already in use on the chosen domain.</em></span></p>
<hr>
<h3>Optional Fields:</h3> 
					<p class="label">Campaign: 		
					<input id="domain" name="campaign" type="text"/>
					<span class="description"><em>A new campaign will be created on your account if the value you specify doesn't already exist.</em></span></p>
					<p class="label">Domain: 		
					<input id="domain" name="domain" type="text"/>
					<span class="description"><em>The default domain will be used if not specified.</em></span></p>
					<p class="label">API Key: 		
					<input id="api_key" name="api_key" type="text" size="60"/>
					<span class="description"><em>The default account will be used if you are logged in to awe.sm. <a href="http://create.awe.sm/login">Login here</a>.</em></span></p>
<hr>
					
					<p><input type="submit"  id="submit" value="Make it awe.sm!" /></p>
				</form>			
			</div>
		</div>
		
			
		<?php

			if (isset($_GET['target'])) {

				$target = $_GET['target'];
$target = urlencode($target);
				
				if (isset($_GET['awesm_id']) &! empty($_GET['awesm_id'])) {
					$awesm_id = $_GET['awesm_id'];
				} else {
					echo "<strong>Please enter a Custom Link Stub.</strong>";
				}
				
				if (!empty($awesm_id)) {
					if (isset($_GET['domain']) &! empty($_GET['domain'])) {
						$domain = $_GET['domain'];
						$domain = str_replace("/", "_", $domain);
						$domain = "&domain=" . $domain;
					} else {
						$domain = '';
					}
					if (isset($_GET['api_key']) &! empty($_GET['api_key'])) {
						$api_key = $_GET['api_key'];
						$api_key = "&api_key=" . $api_key;
					} else {
						$api_key = '';
					}
                                        if (isset($_GET['campaign']) &! empty($_GET['campaign'])) {
						$campaign = $_GET['campaign'];
						$campaign = "&campaign=" . urlencode($campaign);
					} else {
						$campaign = '';
					}

					$api_url = "http://create.awe.sm/url/share?version=1&share_type=static&create_type=awesm-customlinkbuilder&target=" . $target . "&awesm_id=" . $awesm_id . $api_key . $domain . $campaign;
					echo "<script>window.location = '" . $api_url . "'</script>";
				}
				
			} else {
				echo "<strong>Please enter an Original URL.</strong>";
			}
			
		php?>
	</body>
</html>
<?php

class Awesm
{
    
    private $apiKey;
    private $createHost = 'http://create.awe.sm';
    private $statsHosts = 'http://api.awe.sm';
    private $apiVersion = 2;
    
    const EP_CREATE = '/url.';
    const EP_CREATE_AND_REDIRECT = '/url/share';
    const EP_STATS_CLICKS = '/clicks.json';
    const EP_STATS_SHARES = '/shares';
    const EP_STATS_ACTIVITY = '/activity';
    const EP_STATS_CLICKS_LIST = '/clicks/list.json';
    const EP_STATS_SHARES_LIST = '/shares/list';
    const EP_STATS_CLICKS_DETAILS = '/clicks/%s.json';
    
    const DIM_CAMPAIGN = 'campaign';
    const DIM_CHANNEL  = 'channel';
    const DIM_TOOL     = 'tool';
    const DIM_USER     = 'user_id';
    const DIM_NOTES    = 'notes';
    const DIM_PARENT   = 'parent_awesm';
    const DIM_SHARER   = 'sharer_id';
    
    public function __construct($apiKey,$defaults=null)
    {
        $this->apiKey = $apiKey;
        if (array_key_exists('create_host',$defaults)) {
        	$this->createHost = $defaults['create_host'];
        }
        if (array_key_exists('stats_host',$defaults)) {
        	$this->statsHost = $defaults['stats_host'];
        }
        if (array_key_exists('api_version',$defaults)) {
        	$this->apiVersion = $defaults['api_version'];
        }
        // TODO: etc.
    }
    
    public function getClickStats(
    	$after=null,
    	$before=null,
    	$dimensions=null,
    	$interval=null,
    	$groupBy=null
    )
    {
    	$params = array();
    	if(!empty($before)) $params['clicked_before'] = $before;
    	if(!empty($after)) $params['clicked_after'] = $after;
    	if (is_array($dimensions))
    	{
	    	foreach($dimensions as $dimension => $value)
	    	{
	    		$params[$dimension] = $value;
	    	}
    	}
    	if(!empty($interval)) $params['interval'] = $interval;
    	if(!empty($groupBy)) $params['group_by'] = $groupBy;
    	
    	return $this->fetchStats(self::EP_STATS_CLICKS,$params);
    }
    
    public function getClickDetails($identifier)
    {
    	/* http://new-stats-api-1/clicks/
    	 * /awe.sm_53Eul.json
    	 * ?version=2
    	 * &api_key=e3a202c0151fabe207a3cc99ccf3b6bd8a2a1d2eefb5bd72899986acda1080e6
    	 * &interval=hour
    	 * &clicked_after=1279152000
    	 * &clicked_before=1279756800 
    	 */
    }
    
    public function getShareStats(
    	$after=null,
    	$before=null,
    	$dimensions=null,
    	$format='.json'
    )
    {
    	$params = array();
    	if(!empty($before)) $params['created_before'] = $before;
    	if(!empty($after)) $params['created_after'] = $after;
    	
    	if (is_array($dimensions))
    	{
	    	foreach($dimensions as $dimension => $value)
	    	{
	    		$params[$dimension] = $value;
	    	}
    	}
    	return $this->fetchStats(self::EP_STATS_SHARES . $format,$params);
    }
    
    public function getSharesList(
    	$after=null,
    	$before=null,
    	$dimensions=null,
    	$format='.json'
    )
    {
    	$params = array();
    	if(!empty($before)) $params['created_before'] = $before;
    	if(!empty($after)) $params['created_after'] = $after;
    	if (is_array($dimensions))
    	{
	    	foreach($dimensions as $dimension => $value)
	    	{
	    		$params[$dimension] = $value;
	    	}
    	}
    	return $this->fetchStats(self::EP_STATS_SHARES_LIST . $format,$params);
    }
    
    public function v1_getInfoPublic($stub,$domain=null,$format='json')
    {
    	return $this->v1_getInfo($stub,$domain,$format,false);
    }
    
    public function v1_getInfo($stub,$domain=null,$format='json',$bApiKey=true)
    {
    	$params = array();
    	if (!empty($domain)) {
    		$params['domain'] = $domain;
    	}
    	$url = '/url/' . $stub . '.' . $format; 
    	
    	return $this->fetchStats($url,$params,$format,$bApiKey);
    }
    
    public function v1_lookup(
    	$start=null,
    	$end=null,
    	$createdStart=null,
    	$createdEnd=null,
    	$domain=null,
    	$originalUrl=null,
    	$shareType=null,
    	$createType=null,
    	$userId=null,
    	$notes=null,
    	$parentAwesm=null,
    	$sharerId=null,
    	$clicked=null,
    	$details=false,
    	$page=null,
    	$perPage=null,
    	$format='json',
    	$bApiKey=true
    )
    {
    	
    	$url = '/url.'.$format;
    	$params = array(
    		'start_date' => $start,
    		'end_date' => $end,
    		'created_at_start' => $createdStart,
    		'created_at_end' => $createdEnd,
    		'domain' => $domain,
    		'original_url' => $originalUrl,
    		'share_type' => $shareType,
    		'create_type' => $createType,
    		'user_id' => $userId,
    		'notes' => $notes,
    		'parent_awesm' => $parentAwesm,
    		'sharer_id' => $sharerId,
    		'clicked' => $clicked,
    		'details' => $details,
    		'page' => $page,
    		'per_page' => $perPage
    	);
    	
    	return $this->fetchStats($url,$params,$format,$bApiKey);
    }
    
    /**
     * Call a path on the stats host
     * @param string $path Relative; you must pass a leading slash.
     */
    private function fetchStats($base,$params,$responseFormat='json',$bApiKey=true)
    {
    	// insert implicit params
    	$params['version'] = $this->apiVersion;
    	if ($bApiKey) $params['api_key'] = $this->apiKey;
    	
    	// turn params into arguments
    	$kvPairs = array();
    	foreach($params as $key => $value)
    	{
    		if (!empty($value)) $kvPairs[] = urlencode($key). '=' . urlencode($value);
    	}
    	
    	// combine arguments with endpoint
    	$path = '?'.implode('&',$kvPairs);
    	$url = $this->statsHost . $base . $path;
    	error_log("Fetching stats: $url");
    	
    	// call server
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$response = curl_exec($ch);
		error_log("Full response: " . $response);
		
		// parse response
    	if ($responseFormat == 'json')
    	{
    		$result = json_decode($response,true);
    	}
    	else if ($responseFormat == 'xml')
    	{
    		$result = new SimpleXMLElement($response);
    	}
    	else {
    		$result = "Unknown response format";
    	}
    	return $result;
    }
    
    /* @TODO: setters for defaults that will apply to all links created
     * - format
     * - version
     * - share_type
     * - create_type
     * - parent_awesm
     * - domain
     * - sharer_id
     * - user_id
     * - notes
     * - callback
     */
    
    /**
     * Creates a new awesm URL from an original URL
     *
     * @param unknown_type $originalUrl
     * @param unknown_type $params
     * @return AwesmUrl
     */
    public function makeUrl($originalUrl,$params=null)
    {
        return new AwesmUrl($this->apiKey,Awesm::CREATE,$originalUrl,$params);
    }
    
    /**
     * Loads an existing URL
     *
     * @param unknown_type $awesmId
     * @return AwesmUrl
     */
    public function loadUrl($awesmId,$params=null)
    {
        return new AwesmUrl($this->apiKey,Awesm::FETCH,$awesmId,$params);
    }
    
    public function getActivityStats(
        $startDate=null,
        $endDate=null,
        $dimensions=null,
        $interval=null,
        $groupBy=null,
        $format='.json'
    )
    {
        $params = array();
        $this->addIfNotEmpty('start_date',$startDate,$params);
        $this->addIfNotEmpty('end_date',$endDate,$params);
        $this->addIfNotEmpty('interval',$interval,$params);
        $this->addIfNotEmpty('group_by',$groupBy,$params);
        $this->addAllValues($dimensions,$params);
        
        return $this->fetchStats(self::EP_STATS_ACTIVITY . $format,$params);
    }

    function addIfNotEmpty($key,$value,&$array)
    {
        if(!empty($value)) $array[$key] = $value;
    }
    
    function addAllValues($inputArray,&$containerArray)
    {
        if (is_array($inputArray))
        {
            foreach($inputArray as $key => $value)
            {
                $containerArray[$key] = $value;
            }
        }
    }
}


<html>
	<head>
		<title>awe.sm QR Code Generator</title>
		<meta name="description" content="Create QR codes that correspond to awe.sm tracking links.">
	</head>
	<body>


<h1>awe.sm QR Code Generator</h1>

<form method="post">
<p>Enter a URL to encode*: <input type="text" name="url" value="http://" size="45">
<hr>
<h3>Optional Fields:</h3>	
<p>Campaign: <input type="text" name="campaign"><span class="description"> <em>A new campaign will be created on your account if the value you specify doesn't already exist.</em></span></p>
<p>Your awe.sm API key: <input type="text" name="api_key" size="60"><span class="description"> <em>If this is not specified, you will not be able to see stats for your link.</em></span></p>
<hr><input type="submit" value="QR me baby!">
</form>

<?php

if (!empty($_POST['url']))
{
	$url = $_POST['url'];
	if (!empty($_POST['api_key'])) {
		$key = $_POST['api_key'];
	} else {
		$key = "77584eaa42fd83bf6d8765f1962c68f5ff9804a1adf48eb4f64b9198dadaf73a"; // demo
	}
	$campaign = @$_POST['campaign'];
	
	$response = file_get_contents("http://api.awe.sm/url.json?v=2&key=$key&channel=qr&tool=tW9Nkd&campaign=$campaign&url=".urlencode($url));
	$result = json_decode($response,true);
	$awesmUrl = $result['url']['awesm_url'];
	$encodedUrl = urlencode($awesmUrl);
	error_log("Encoded url: $encodedUrl");
	?>
	<hr>
	<h1>Done!</h1>
	
	<p>Your link is <a href="<?=$awesmUrl?>"><?=$awesmUrl?></a>. Your QR code is:
		
	<p><img src="php/qr_img.php?d=<?=$encodedUrl?>"></p>

	<p>Save this image to your desktop, then you can <a href="http://zxing.org/w/decode.jspx">decode it</a>.
	<?php
	
}

?>



<html>
	<head>
		<title>awe.sm Link Creation Form</title>
		<meta name="description" content="A form for existing awe.sm users to create tracking links.">
	</head>
	<body>
		<div id="container">
			<h1>awe.sm Link Creation Form</h1>
						
			<!-- Bookmarklet code to be completed
			<div id="bookmarklet">
				<p class="label">Grab a bookmarklet:</p>
				<p class="label"><a href="javascript:(function()%20{window.open('http://developers.awe.sm/tools/customlinkbuilder/?target='+encodeURIComponent(location.href));})();">awe.sm Custom</a></p>
				<p class="description">Drag the above link into your bookmarks bar.</p>
			</div> 
			-->


			<div id="content">
				<form method="get">
<h3>Basic:</h3>
					<p class="label">Original URL*:
					<input id="target" name="target" type="text" /></p>

					<p class="label">Campaign: 		
					<input id="domain" name="campaign" type="text"/>
					<span class="description"><em>A new campaign will be created on your account if the value you specify doesn't already exist.</em></span></p>

					<p class="label">Channel: 		
					<input id="domain" name="channel" type="text"/>
					<span class="description"><em>Channel will be 'copypaste' if not specified. [If you have Google Analytics integration enabled, will be used for <a href="http://www.google.com/support/analytics/bin/answer.py?answer=55518">utm_medium</a> value.]</em></span></p>

					<p class="label">Tool: 		
					<input id="domain" name="tool" type="text"/>
					<span class="description"><em>Tool will be 'awesm-createform' if not specified. [If you have Google Analytics integration enabled, will be used for <a href="http://www.google.com/support/analytics/bin/answer.py?answer=55518">utm_content</a> value.]</em></span></p>

					<p class="label">Notes: 		
					<input id="domain" name="notes" type="text"/>
					<span class="description"><em>Notes will be blank if not specified. Notes values are only available via <a href="http://developers.awe.sm">our APIs</a>. [If you have Google Analytics integration enabled, will be used for <a href="http://www.google.com/support/analytics/bin/answer.py?answer=55518">utm_campaign</a> value.]</em></span></p>

<hr>
<h3>Advanced:</h3> 

					<p class="label">Custom Link Stub: 		
					<input id="awesm_id" name="awesm_id" type="text"/>
					<span class="description"><em>Will use the next available stub if not specified. An error will be returned if the stub is already in use on the chosen domain.</em></span></p>

					<p class="label">Domain: 		
					<input id="domain" name="domain" type="text"/>
					<span class="description"><em>The default domain will be used if not specified.</em></span></p>
					<p class="label">API Key: 		
					<input id="api_key" name="api_key" type="text" size="60"/>
					<span class="description"><em>The default account will be used if you are logged in to awe.sm. <a href="http://create.awe.sm/login">Login here</a>.</em></span></p>
<hr>
					
					<p><input type="submit"  id="submit" value="Make it awe.sm!" /></p>
				</form>			
			</div>
		</div>
		
			
		<?php

			if (isset($_GET['target'])) {

				$target = $_GET['target'];
				$target = urlencode($target);
				
				if (isset($_GET['domain']) &! empty($_GET['domain'])) {
					$domain = $_GET['domain'];
					$domain = str_replace("/", "_", $domain);
					$domain = "&domain=" . $domain;
				} else {
					$domain = '';
				}
					
				if (isset($_GET['api_key']) &! empty($_GET['api_key'])) {
					$api_key = $_GET['api_key'];
					$api_key = "&api_key=" . $api_key;
				} else {
					$api_key = '';
				}
               	
				if (isset($_GET['campaign']) &! empty($_GET['campaign'])) {
					$campaign = $_GET['campaign'];
					$campaign = "&campaign=" . urlencode($campaign);
				} else {
					$campaign = '';
				}
                
				if (isset($_GET['notes']) &! empty($_GET['notes'])) {
					$notes = $_GET['notes'];
					$notes = "&notes=" . $notes;
				} else {
					$notes = '';
				}
                
				if (isset($_GET['awesm_id']) &! empty($_GET['awesm_id'])) {
					$awesm_id = $_GET['awesm_id'];
					$awesm_id = "&awesm_id=" . $awesm_id;
				} else {
					$awesm_id = '';
				}
                
				if (isset($_GET['channel']) &! empty($_GET['channel'])) {
					$channel = $_GET['channel'];
					$channel = "&share_type=" . $channel;
				} else {
					$channel = '&share_type=copypaste';
				}
                
				if (isset($_GET['tool']) &! empty($_GET['tool'])) {
					$tool = $_GET['tool'];
					$tool = "&create_type=" . $tool;
				} else {
					$tool = 'create_type=awesm-createform';
				}

				$api_url = "http://create.awe.sm/url/share?version=1&target=" . $target . $channel . $tool . $api_key . $notes . $domain . $awesm_id . $campaign;
				echo "<script>window.location = '" . $api_url . "'</script>";
				
			} else {
				echo "<strong>Please enter an Original URL.</strong>";
			}
			
		php?>
	</body>
</html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<title>awe.sm Top Sharers</title>
		<style>
			body {
				font-family: Georgia;
			}
		</style>		
	</head>
	<body>

		<div id="container">
		

			<div id="content">
				<form method="get">
					<p class="label">awe.sm API Key*:
					<input id="api_key" name="api_key" type="text" /></p>
					<p class="label">Period Start Date* (YYYY-MM-DD):
					<input id="domain" name="start_date" type="text" /></p>
					<p class="label">Period End Date* (YYYY-MM-DD):
					<input id="domain" name="end_date" type="text" /></p>

					<p><input type="submit"  id="submit" value="Get my Top Sharers" /></p>
				</form>			
			</div>
		</div>
		


		<?php

			function awesm_lookup ($api_key, $start_date, $end_date) {
				
				$awesm_request_url = "http://api.awe.sm/clicks.json?version=2&channel=twitter&group_by=sharer_id&api_key=". $api_key . "&clicked_after=" . $start_date . "&clicked_before=" . $end_date;
				
				//echo "awesm_request_url: " . $awesm_request_url . "<br>";
				
				$awesm_request = curl_init();	
				curl_setopt($awesm_request, CURLOPT_URL, $awesm_request_url);
				curl_setopt($awesm_request, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($awesm_request, CURLOPT_TIMEOUT, 90);
				$awesm_request_response = curl_exec($awesm_request);
				$awesm_request_response_code = curl_getinfo($awesm_request, CURLINFO_HTTP_CODE);
				curl_close($awesm_request);
				// Verify HTTP response code received
				if ($awesm_request_response_code != 200) {
					echo "[Error loading data from awe.sm]";
				} else {
					$awesm_response = json_decode($awesm_request_response,true);
					return($awesm_response);
				}
			}

			function get_awesm_urls ($api_key, $sharer_id, $start_date, $end_date) {
				
				$awesm_request_url = "http://api.awe.sm/shares/list.json?version=2&api_key=". $api_key . "&clicked_after=" . $start_date . "&clicked_before=" . $end_date . "&sharer_id=" . $sharer_id;
				
				//echo "awesm_request_url: " . $awesm_request_url . "<br>";
				
				$awesm_request = curl_init();	
				curl_setopt($awesm_request, CURLOPT_URL, $awesm_request_url);
				curl_setopt($awesm_request, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($awesm_request, CURLOPT_TIMEOUT, 90);
				$awesm_request_response = curl_exec($awesm_request);
				$awesm_request_response_code = curl_getinfo($awesm_request, CURLINFO_HTTP_CODE);
				curl_close($awesm_request);
				// Verify HTTP response code received
				if ($awesm_request_response_code != 200) {
					echo "[Error loading data from awe.sm]";
				} else {
					$awesm_response = json_decode($awesm_request_response,true);
					return($awesm_response);
				}
			}

			function get_tweets ($domain, $awesm_id) {
				
				//Add _ for / sub on $domain	
				
				$twitter_request_url = "http://search.twitter.com/search.json?q=". urlencode($domain . "/" . $awesm_id);
				//echo "twitter_request_url: " . $twitter_request_url . "<br>";
								
				$twitter_request = curl_init();	
				curl_setopt($twitter_request, CURLOPT_URL, $twitter_request_url);
				curl_setopt($twitter_request, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($twitter_request, CURLOPT_TIMEOUT, 200);
				curl_setopt($twitter_request,CURLOPT_USERAGENT,'Testing Some Stuff');
				$twitter_request_response = curl_exec($twitter_request);
				$twitter_request_response_code = curl_getinfo($twitter_request, CURLINFO_HTTP_CODE);
				curl_close($twitter_request);
				
				//print_r($twitter_request_response);
				
				// Verify HTTP response code received
				if ($twitter_request_response_code != 200) {
					echo "[Error loading data from Twitter]";
				} else {
					$twitter_response = json_decode($twitter_request_response,true);
					return($twitter_response);
				}
			}

    		if (!empty($_GET['start_date']) && !empty($_GET['end_date']) && !empty($_GET['api_key'])) {

				$api_key = $_GET['api_key'];
				$start_date = $_GET['start_date'];
				$end_date = $_GET['end_date'];
				
				$click_groups_response = awesm_lookup($api_key, $start_date, $end_date);
				//print_r($click_groups_response);
				
				$sharers = array();
				
				foreach ($click_groups_response['groups'] as $key => $value) {
					if ($value['clicks'] > 1) {
						$sharers[$key] = $value;
						
						if (empty($value['sharer_id'])) continue; // skip null sharer ids
						
						$awesm_urls = get_awesm_urls($api_key, $value['sharer_id'], $start_date, $end_date);
						
						//print_r($awesm_urls);
						
						$sharers[$key]['awesm_urls'] = $awesm_urls['share_list'];
						
						foreach ($sharers[$key]['awesm_urls'] as $awesmUrlIndex => $awesmUrlValue) {
							$tweets = get_tweets($awesmUrlValue['domain'], $awesmUrlValue['awesm_id']);
							$sharers[$key]['awesm_urls'][$awesmUrlIndex]['tweets'] = $tweets['results'];
						}
					}
				}
	
			} else {
				echo "<strong>Please enter a Start Date, End Date, and API Key.</strong>";
			}
	
			?><ul><?php
			foreach($sharers as $click)
			{
				$clicks = $click['clicks'];
				if (isset($click['awesm_urls']) && !empty($click['awesm_urls']) )
				{
					foreach($click['awesm_urls'] as $awesm_url)
					{
						foreach($awesm_url['tweets'] as $tweet)
						{
							printf(
								'<li><a href="%s"><img src="%s" alt="%s" border="0"></a> <span class="tweet">%s</span> <span class="clicks">(%s)</span></li>',
								'http://twitter.com/'.$tweet['from_user'],
								$tweet['profile_image_url'],
								$tweet['from_user'],
								$tweet['text'],
								$clicks
							);
						}
					}
				} 
			}
			?></ul><?php

	
		?>
	</body>
</html>